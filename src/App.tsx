import { Container, Paper, Typography } from '@mui/material';
import { GridColDef } from '@mui/x-data-grid';
import { useAsync, useAsyncFn } from 'react-use';
import { useState } from 'react';
import { PesoFornecedor } from './@types';
import api from './services/api';
import socket from './services/socket';
import DataGrid from './components/Datagrid';

const columns: GridColDef<PesoFornecedor>[] = [
  { field: 'id', headerName: 'ID', flex: 1 },
  { field: 'peso', headerName: 'Peso (kg)', flex: 1 },
  {
    field: 'supplier',
    headerName: 'Fornecedor',
    flex: 1,
    renderCell: ({ row }) => row.supplier.nome,
  },
];

const App: React.FC = () => {
  const [pesos, setPesos] = useState<PesoFornecedor[]>([]);

  const [{ loading }, getPesos] = useAsyncFn(async () => {
    try {
      const { data } = await api.get<PesoFornecedor[]>('Pesos');

      setPesos(data);
    } catch (error) {
      console.error('Erro ao buscar os pesos.');
    }
  }, []);

  useAsync(async () => {
    getPesos();

    socket.on('newPeso', () => {
      getPesos();
    });

    return () => {
      socket.off('newPeso');
    };
  }, []);

  return (
    <Container>
      <Typography variant="h4" component="h1" gutterBottom>
        Lista de Pesos
      </Typography>

      <Paper style={{ height: 400, width: '100%' }}>
        <DataGrid loading={loading} rows={pesos} columns={columns} />
      </Paper>
    </Container>
  );
};

export default App;
