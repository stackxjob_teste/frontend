export interface Fornecedor {
  id: string;
  nome: string;
  descricao: string;
}
