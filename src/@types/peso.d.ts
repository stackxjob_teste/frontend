import { Fornecedor } from './fornecedor';

export type Peso = {
  id: string;
  peso: number;
};

export type PesoFornecedor = Peso & {
  supplier: Fornecedor;
};
