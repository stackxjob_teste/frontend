import { Stack } from '@mui/material';
import { DataGrid as MuiDataGrid, GridColDef } from '@mui/x-data-grid';

type DataGridProps = {
  loading: boolean;
  rows: unknown[];
  columns: GridColDef[];
};

const noRowsOverlay = () => (
  <Stack height="100%" alignItems="center" justifyContent="center">
    Sem dados
  </Stack>
);

const noResultsOverlay = () => (
  <Stack height="100%" alignItems="center" justifyContent="center">
    Os filtros não retornaram resultados
  </Stack>
);

const DataGrid: React.FC<DataGridProps> = ({ loading, rows, columns }) => (
  <MuiDataGrid
    loading={loading}
    rows={rows}
    columns={columns}
    pagination
    paginationMode="client"
    slots={{
      noRowsOverlay,
      noResultsOverlay,
    }}
  />
);

export default DataGrid;
