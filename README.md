# Rodar o Projeto Frontend

## Pré requisitos:
* Backend previamente rodando
* Node
* Yarn

## Comandos para rodar o projeto
Antes de rodar o projeto de fato, você precisar clonar e renomear o arquivo `.env.example` para `.env`. Realize as configurações caminho da API e WebSocket necessárias. Para conveniência, o arquivo `.env.example` está configurado corretamente.

Instale as dependências do projeto, utilizando o comando:
```
yarn
```

### Subindo o projeto
Para subir o projeto, basta rodar o comando abaixo:
```
yarn dev
```
O projeto estará exposto na URL [clicando aqui](http://localhost:5173).